<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

$data = [
    [
        'name' => 'Хирургия',
        'sections' => [
            [
                'name' => 'Пластическая хирургия',
                'items' => [
                    [
                        'name' => 'Василий Петрович',
                    ],
                    [
                        'name' => 'Ирина Соловьева',
                    ],
                    [
                        'name' => 'Никита Прокопенко',
                    ],
                ]
            ],
            [
                'name' => 'Травмотология',

            ],
            [
                'name' => 'Челюстно-лицевая хирургия',
                'items' => [
                    [
                        'name' => 'Дмитрий Игоревич',
                    ],
                ]
            ],
        ],
    ],

    [
        'name' => 'Стомотология',
        'items' => [
            [
                'name' => 'Лариса Ивановна',
            ],
            [
                'name' => 'Людмила Григорьевна',
            ],
        ]
    ],

    [
        'name' => 'Офтальмология',
        'sections' => [
            [
                'name' => 'Детская офтальмология',
                'items' => [
                    [
                        'name' => 'Счастливая Анна Иванова',
                    ],
                    [
                        'name' => 'Мария Захарова',
                    ],
                    [
                        'name' => 'Василий Успецкий',
                    ],
                ]
            ],
            [
                'name' => 'Взрослая офтальмология',
                'items' => [
                    [
                        'name' => 'Александр Маренич',
                    ],
                ]
            ],
        ],
    ],

];

$sectionIds = [];
insertSectionsInDb($data);

function insertSectionsInDb($items, $section = false) {
    foreach($items as $item) {
        $arFields = array(
            'IBLOCK_ID' => 1,
            'IBLOCK_SECTION_ID' => $section,
            'NAME' => $item['name'],
        );
        $ob = new CIBlockSection();
        $id = $ob->Add($arFields);
        if($id) {
            if(!empty($item['items'])) {
                insertItemsInDb($item['items'], $id);
            }

            if(!empty($item['sections'])) {
                insertSectionsInDb($item['sections'], $id);
            }
        }
    }
}

function insertItemsInDb($items, $section) {
    foreach($items as $item) {
        $arFields = array(
            'IBLOCK_ID' => 1,
            'IBLOCK_SECTION_ID' => $section,
            'NAME' => $item['name'],
        );
        $ob = new CIBlockElement();
        $id = $ob->Add($arFields);
    }
}