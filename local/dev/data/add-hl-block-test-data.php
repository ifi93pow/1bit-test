<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

$hlIblockId = 2;

$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlIblockId)->fetch();
$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
/**
 * @var \Bitrix\Main\Entity\DataManager $hl
 */
$hl = new $entity_data_class();

$from = 50;
$to = 59;

$times = [
    '10-16',
    '10-14,15-19',
    '9:30-14:00',
    '9:30-13:30,15:30-18:30'
];

for ($i = $from; $i < $to; $i++) {

    $activeFrom = new DateTime();
    $activeFrom->modify(
        (rand(0, 100) < 50 ? '+' : '-')
        . rand(1, 20)
        . (rand(0, 100) < 50 ? 'day' : 'month'));

    for ($j = 0; $j < 10; $j++) {
        $activeTo = clone $activeFrom;
        $activeTo->modify('+1 month');

        $arFields = array(
            'UF_DOCTOR_ID' => $i,
            'UF_ACTIVE' => true,
            'UF_ACTIVE_FROM' => FormatDate('FULL', $activeFrom->getTimestamp()),
            'UF_ACTIVE_TO' => FormatDate('FULL', $activeTo->getTimestamp()),
        );
        for ($k = 1; $k <= 7; $k++) {
            if (rand(0, 100) < 70) {
                $arFields['UF_DAY_' . $k] = false;
            } else {
                $arFields['UF_DAY_' . $k] = true;
                $arFields['UF_DAY_' . $k . '_TIME'] = $times[rand(0, count($times) - 1)];
            }
        }

        $activeFrom = $activeTo;
        $activeFrom->modify('+1 month');

        $hl->add($arFields);
    }

}


