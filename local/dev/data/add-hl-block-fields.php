<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('highloadblock');

$hlIblockId = 1;

use Bitrix\Highloadblock as HL;

// select data
//$rsData = HL\HighloadBlockTable::getList(array(
//    "select" => $lAdmin->GetVisibleHeaderColumns(),
//    "order" => array($by => strtoupper($order))
//));
//print_r($rsData->fetchAll());

$langs = ['ru'];
$labelKeys = ['EDIT_FORM_LABEL', 'LIST_COLUMN_LABEL', 'LIST_FILTER_LABEL'];

$arFields1 = array(
    'ENTITY_ID' => 'HLBLOCK_2',
    'USER_TYPE_ID' => 'boolean',
    'SETTINGS' => array(
        'DEFAULT_VALUE' => 0
    )
);
$arFields2 = array(
    'ENTITY_ID' => 'HLBLOCK_2',
    'USER_TYPE_ID' => 'string',
);
$days = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');

for ($i = 1; $i <= 7; $i++) {
    foreach ($labelKeys as $key) {
        foreach ($langs as $lang) {
            $arFields1[$key][$lang] = $days[$i - 1];
        }
    }

    $arFields1['FIELD_NAME'] = 'UF_DAY_' . $i;

    $ob = new CUserTypeEntity();
    $ob->Add($arFields1);


    foreach ($labelKeys as $key) {
        foreach ($langs as $lang) {
            $arFields2[$key][$lang] = 'Время приема (' . $days[$i - 1] . ')';
        }
    }

    $arFields2['FIELD_NAME'] = 'UF_DAY_' . $i . '_TIME';

    $ob = new CUserTypeEntity();
    $ob->Add($arFields2);
}
