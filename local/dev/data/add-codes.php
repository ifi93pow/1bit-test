<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

$rs = CIBlockSection::GetList(array(), array(), false, array('ID', 'NAME'));
while($ar = $rs->Fetch()) {
    $ob = new CIBlockSection();
    $ob->Update($ar['ID'], array('CODE' => getTranslitCode($ar['NAME'])));
}

function getTranslitCode($str)
{
    $arParams = array('replace_space' => '-', 'replace_other' => '-', 'max_len' => 100);
    $result = CUtil::translit($str, 'ru', $arParams);
    return trim($result, '-');
}