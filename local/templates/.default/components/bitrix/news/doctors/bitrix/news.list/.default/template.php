<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
showSections($arResult['SECTIONS'], $arResult);
?>
<? function showSections($sections, &$arResult)
{ ?>
    <ul>
    <? foreach ($sections as $section) { ?>
    <? $isCurrentSection = !empty($arResult['SECTION']) && $arResult['SECTION']['ID'] === $section['ID']; ?>
    <? $isEmptySection = empty($section['sections']) && empty($section['items']) ?>
    <? if ($isEmptySection) continue ?>

    <? if (!$isCurrentSection) { ?>
        <li>
    <? } ?>

    <? if (!$isCurrentSection) { ?>
        <h<?= $section['DEPTH_LEVEL'] + 1 ?>>
            <a href="<?= $section["SECTION_PAGE_URL"] ?>"><?= $section["NAME"] ?></a>

        </h<?= $section['DEPTH_LEVEL'] + 1 ?>>
    <? } ?>

    <? if (!empty($section['items'])) { ?>
        <ol>
            <? foreach ($section['items'] as $arItem): ?>
                <li class="news-item">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                </li>
            <? endforeach; ?>
        </ol>
    <? } ?>

    <? if (!empty($section['sections'])) showSections($section['sections'], $arResult); ?>

    <? if (!$isCurrentSection) { ?>
        </li>
    <? } ?>

<? } ?>
    </ul>
<? } ?>
