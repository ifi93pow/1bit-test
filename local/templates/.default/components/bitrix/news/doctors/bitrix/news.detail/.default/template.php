<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
    <?= $arResult["DETAIL_TEXT"]; ?>
<? else: ?>
    <?= $arResult["PREVIEW_TEXT"]; ?>
<? endif ?>

<p>Для регистрации на прием выберите доступную дату и время.</p>
<ul class="js-schedule schedule">
    <? foreach ($arResult['SCHEDULE'] as $date => $times) { ?>
        <li>
            <?= $date ?>
            <ul class="schedule__times">
                <? foreach ($times as $minutes) { ?>
                    <? $time = sprintf('%02d', floor($minutes / 60)) . ':' . sprintf('%02d', $minutes % 60) ?>
                    <li class="<? if (isset($arResult['SCHEDULE_REGISTERED'][$date][$minutes])) { ?>schedule__time--disabled<? } else { ?>js-time<? } ?> schedule__time"
                        data-date="<?= $date ?>" data-time="<?= $time ?>">
                        <?= $time ?>
                    </li>
                <? } ?>
            </ul>
        </li>
    <? } ?>
</ul>