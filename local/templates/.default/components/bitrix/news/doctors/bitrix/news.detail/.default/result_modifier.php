<?php

\Bitrix\Main\Loader::includeModule('app');
$scheduleGetter = new App\ScheduleGetter();
$arResult['SCHEDULE'] = $scheduleGetter->getDoctorScheduleReceipts($arResult['ID']);
$arResult['SCHEDULE_REGISTERED'] = $scheduleGetter->getDoctorScheduleReceiptsRegistered($arResult['ID']);

// other
$arResult['TITLE'] = $arResult['NAME'];
$this->__component->setResultCacheKeys(array('TITLE'));