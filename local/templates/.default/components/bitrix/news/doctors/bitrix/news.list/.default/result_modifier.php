<?php

// get sections
$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
if (!empty($arResult['SECTION']['PATH'])) {
    $section = end($arResult['SECTION']['PATH']);
    $section['PATH'] = $arResult['SECTION']['PATH'];
    $arResult['SECTION'] = $section;

    $arFilter['>LEFT_MARGIN'] = $arResult['SECTION']['LEFT_MARGIN'];
    $arFilter['<RIGHT_MARGIN'] = $arResult['SECTION']['RIGHT_MARGIN'];
}

$rs = CIBlockSection::GetList(
    array('SORT' => 'ASC', 'NAME' => 'ASC'),
    $arFilter,
    false,
    array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL', 'DEPTH_LEVEL')
);
$sections = $sectionsById = array();
while ($ar = $rs->GetNext(true, false)) {
    $sectionsById[$ar['ID']] = $ar;
    $sections[] = &$sectionsById[$ar['ID']];
}

// put sections into sections
foreach ($sections as $i => &$section) {
    if (!empty($section['IBLOCK_SECTION_ID']) && isset($sectionsById[$section['IBLOCK_SECTION_ID']])) {
        if (isset($sectionsById[$section['IBLOCK_SECTION_ID']]['sections'])) {
            $sectionsById[$section['IBLOCK_SECTION_ID']]['sections'][] = &$sections[$i];
        } else {
            $sectionsById[$section['IBLOCK_SECTION_ID']]['sections'] = [&$sections[$i]];
        }
        unset($sections[$i]);
    }
}
$sections = array_values($sections);

// put items into sections
foreach ($arResult['ITEMS'] as $i => $item) {
    if (!empty($item['IBLOCK_SECTION_ID']) && isset($sectionsById[$item['IBLOCK_SECTION_ID']])) {
        if (isset($sectionsById[$item['IBLOCK_SECTION_ID']]['items'])) {
            $sectionsById[$item['IBLOCK_SECTION_ID']]['items'][] = $item;
        } else {
            $sectionsById[$item['IBLOCK_SECTION_ID']]['items'] = [$item];
        }
        unset($arResult['ITEMS'][$i]);
    }
}

// save sections
$arResult['SECTIONS'] = $sections;

// other
$arResult['TITLE'] = $arResult['SECTION'] ? $arResult['SECTION']['NAME'] : $arResult['NAME'];
$this->__component->setResultCacheKeys(array('TITLE', 'SECTION'));