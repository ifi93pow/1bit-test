<?
$arFields = $this->__component->fields;
?>
<? if($arResult['ERROR']) { ?>
    <div class="error-text"><?= $arResult['ERROR'] ?></div>
<? } ?>
<form class="form-register js-form-register" method="post" action="">
    <input type="hidden" name="doctor" value="<?= $arParams['DOCTOR_ID'] ?>">
    <input type="hidden" name="form" value="register">
    <?= bitrix_sessid_post() ?>

    <p>
        <label class="form-register__label" for="register-name">ФИО</label>
        <input type="text" name="name" id="register-name" value="<?= $arFields['name'] ?>">
    </p>

    <p>
        <label class="form-register__label" for="register-date">Дата приема</label>
        <input type="text" name="date" id="register-date" class="js-date" value="<?= $arFields['date'] ?>">
    </p>

    <p>
        <label class="form-register__label" for="register-time">Время приема</label>
        <input type="text" name="time" id="register-time" class="js-time" value="<?= $arFields['time'] ?>">
    </p>
    <p>
        <label class="form-register__label" for="register-text">Текст</label>
        <textarea name="text" id="register-text"><?= $arFields['text'] ?></textarea>
    </p>
    <p>
        <input type="submit" value="Записаться">
    </p>
</form>