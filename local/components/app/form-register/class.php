<?php

class CAppFormRegister extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        CModule::IncludeModule('app');

        $arParams['IS_AJAX'] = App\App::isAjax();

        return $arParams;
    }

    public function executeComponent()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_REQUEST['form'] == 'register') {
            try {
                if (!check_bitrix_sessid()) throw new Exception('Некорректная сессия, попробуйте заполнить форму заново.');
                if (!$this->arParams['DOCTOR_ID']) throw new Exception('Не указан доктор');

                $this->fields = array(
                    'name' => $_REQUEST['name'],
                    'date' => $_REQUEST['date'],
                    'time' => $_REQUEST['time'],
                    'text' => $_REQUEST['text']
                );

                foreach ($this->fields as $key => $val) {
                    if (!$val) throw new Exception("Поле $key обязательно для заполнения");
                }

                $registerTime = $this->fields['date'] . ' ' . $this->fields['time'];
                $registerDateTime = new DateTime($registerTime);
                $registerUtime = $registerDateTime->getTimestamp();
                if ($registerUtime < time()) throw new Exception('Некорректный формат время приема');
                $this->fields['full-time'] = FormatDate('FULL', $registerUtime);

                require 'handler-form-register.php';
                LocalRedirect('/success/');
            } catch (Exception $e) {
                $this->arResult['ERROR'] = $e->getMessage();
                $this->includeComponentTemplate();
            }
        } elseif ($this->startResultCache()) {
            $this->setResultCacheKeys(array());
            $this->includeComponentTemplate();
        }
    }
}