<?php

// check time exist
\Bitrix\Main\Loader::includeModule('app');
$scheduleGetter = new App\ScheduleGetter();
$schedule = $scheduleGetter->getDoctorScheduleReceipts($this->arParams['DOCTOR_ID']);
list($hours, $minutes) = explode(':', $this->fields['time']);
if (!in_array($hours * 60 + $minutes, $schedule[$this->fields['date']])) {
    throw new Exception('Такого времени у доктора в расписании нет');
}

// add registry row
$arFields = array(
    'UF_DOCTOR_ID' => $this->arParams['DOCTOR_ID'],
    'UF_ACTIVE' => true,
    'UF_TIME' => $this->fields['full-time'],
    'UF_NAME' => $this->fields['name'],
    'UF_TEXT' => $this->fields['text'],
);

\Bitrix\Main\Loader::includeModule('highloadblock');

$hlIblockId = 1;

$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlIblockId)->fetch();
$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
/**
 * @var \Bitrix\Main\Entity\DataManager $hl
 */
$hl = new $entity_data_class();

// check, if someone register yet
$arFilter = $arFields;
unset($arFilter['UF_NAME']);
unset($arFilter['UF_TEXT']);
$count = $hl->getList(array('filter' => $arFilter))->getSelectedRowsCount();
if ($count > 0) throw new Exception('Заданное время у этого доктора уже занято.');


$hl->add($arFields);


// clear cache
App\ScheduleHandlers::clearDoctorsCache();


// TODO: add here CEvent::Send