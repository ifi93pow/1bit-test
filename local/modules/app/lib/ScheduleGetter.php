<?php

namespace App;

class ScheduleGetter
{
    public $receiptMinutes = 30;

    function __construct()
    {
        \Bitrix\Main\Loader::includeModule('highloadblock');

        $this->curTime = new \DateTime();
        $this->endTime = clone $this->curTime;
        $this->endTime = $this->endTime->modify('+5 month');
    }

    function getDoctorSchedule($doctorId)
    {
        $scheduleData = $this->getScheduleRawData($doctorId);
        $scheduleData = $this->getScheduleData($scheduleData);
        return $scheduleData;
    }

    private function getScheduleRawData($doctorId)
    {
        $hlIblockId = 2;

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlIblockId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        /**
         * @var \Bitrix\Main\Entity\DataManager $hl
         */
        $hl = new $entity_data_class();

        $scheduleData = $hl->getList([
            'order' => [
                'UF_ACTIVE_FROM' => 'ASC',
                'UF_ACTIVE_TO' => 'ASC',
                'ID' => 'ASC'
            ],
            'filter' => [
                'UF_DOCTOR_ID' => $doctorId,
                'UF_ACTIVE' => true,
                ['LOGIC' => 'OR', 'UF_ACTIVE_TO' => false, '>UF_ACTIVE_TO' => FormatDate('FULL', $this->curTime->getTimestamp())],
//                ['LOGIC' => 'OR', 'UF_ACTIVE_FROM' => false, '<UF_ACTIVE_FROM' => FormatDate('FULL', $this->endTime->getTimestamp())],

            ]
        ])->fetchAll();

        return $scheduleData;
    }

    private function getScheduleData($scheduleData)
    {
        $schedule = [];
        foreach ($scheduleData as $row) {
            /**
             * @var $row ['UF_ACTIVE_FROM'] Bitrix\Main\Type\DateTime
             */
            $activeFromTime = $row['UF_ACTIVE_FROM']
                ? (new \DateTime())->setTimestamp($row['UF_ACTIVE_FROM']->getTimestamp())
                : $this->curTime;

            $activeToTime = $row['UF_ACTIVE_TO']
                ? (new \DateTime())->setTimestamp($row['UF_ACTIVE_TO']->getTimestamp())
                : $this->endTime;
            if ($activeToTime > $this->endTime) {
                $activeToTime = $this->endTime;
            }

            $time = clone $activeFromTime;
            while ($time < $activeToTime) {
                $dayOfWeek = $time->format('N'); // 1-7
                if ($row["UF_DAY_$dayOfWeek"] && $row["UF_DAY_{$dayOfWeek}_TIME"]) {
                    $schedule[$time->format('Y-m-d')] = $this->divideStringTimeToArray($row["UF_DAY_{$dayOfWeek}_TIME"]);
                }
                $time->modify('+1 day');
            }
        }
        return $schedule;
    }

    private function divideStringTimeToArray($str)
    {
        $result = preg_split('/[,; ]/', $str);
        return $result;
    }

    function getDoctorScheduleRegister($doctorId)
    {
        $scheduleData = $this->getDoctorScheduleRegisterRawData($doctorId);
        $scheduleData = $this->getDoctorScheduleRegisterData($scheduleData);
        return $scheduleData;
    }

    private function getDoctorScheduleRegisterRawData($doctorId)
    {
        $hlIblockId = 1;

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlIblockId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        /**
         * @var \Bitrix\Main\Entity\DataManager $hl
         */
        $hl = new $entity_data_class();

        $scheduleData = $hl->getList([
            'order' => [
                'UF_TIME' => 'ASC',
                'ID' => 'ASC'
            ],
            'filter' => [
                'UF_DOCTOR_ID' => $doctorId,
                'UF_ACTIVE' => true,
                '>UF_TIME' => FormatDate('FULL', $this->curTime->getTimestamp())
            ]
        ])->fetchAll();

        return $scheduleData;
    }

    private function getDoctorScheduleRegisterData($scheduleData)
    {
        $schedule = [];

        foreach ($scheduleData as $row) {
            /**
             * @var $row ['UF_TIME'] Bitrix\Main\Type\DateTime
             */
            if (!$row['UF_TIME']) continue;
            $time = (new \DateTime())->setTimestamp($row['UF_TIME']->getTimestamp());

            $schedule[$time->format('Y-m-d')][$time->format('G') * 60 + $time->format('i')] = true;
        }

        return $schedule;
    }

    function getDoctorScheduleReceipts($doctorId)
    {
        $schedule = $this->getDoctorSchedule($doctorId);
//        $scheduleRegister = $this->getDoctorScheduleRegister($doctorId);
        $result = $this->calcDoctorScheduleReceipts($schedule);
        return $result;
    }

    function getDoctorScheduleReceiptsRegistered($doctorId)
    {
        $scheduleRegister = $this->getDoctorScheduleRegister($doctorId);
        return $scheduleRegister;
    }

    private function calcDoctorScheduleReceipts($schedule)
    {
        $result = [];

        foreach ($schedule as $day => $times) {
            foreach ($times as $time) {
                $arTime = preg_split('/-/', $time);
                if (count($arTime) != 2) continue;
                $arTime[0] = $this->getMinutesFromTimeString($arTime[0]);
                $arTime[1] = $this->getMinutesFromTimeString($arTime[1]);
                if ($arTime[0] > $arTime[1]) continue;

                for ($time = $arTime[0]; $time < $arTime[1]; $time += $this->receiptMinutes) {
                    $result[$day][] = $time;
                }
            }
        }

        return $result;
    }

    private function getMinutesFromTimeString($str)
    {
        $time = explode(':', $str);
        if (!isset($time[1])) {
            $time[1] = 0;
        }
        $result = $time[0] * 60 + $time[1];
        return $result;
    }
}