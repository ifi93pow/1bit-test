<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

if (class_exists('app')) return;

class app extends \CModule
{
    public $MODULE_ID = __CLASS__;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_CSS;

    function __construct()
    {
        $this->MODULE_NAME = 'App';
        $this->MODULE_DESCRIPTION = 'Кастомный модуль сайта';
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "App";
        $this->PARTNER_URI = "http://localhost";
        $this->MODULE_VERSION = '1.0.0';
        $this->MODULE_VERSION_DATE = '2016-08-13 22:54:55';
    }

    function DoInstall()
    {
        global $DB, $APPLICATION, $step;
        ModuleManager::registerModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Установка модуля ' . $this->MODULE_NAME, __DIR__ . '/step.php');
        return true;
    }

    function DoUninstall()
    {
        global $DB, $APPLICATION, $step;
        ModuleManager::unregisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Удаление модуля ' . $this->MODULE_NAME, __DIR__ . '/unstep.php');
        return true;
    }
}