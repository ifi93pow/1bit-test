<?php

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(
    "app",
    array(
        'App\ScheduleGetter' => 'lib/ScheduleGetter.php',
        'App\ScheduleHandlers' => 'lib/ScheduleHandlers.php',
        'App\App' => 'lib/App.php',
    )
);