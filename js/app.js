(function () {
    $('.js-schedule').on('click', '.js-time', function () {
        var $this = $(this);

        var $formRegister = $('.js-form-register');
        var $formRegisterDate = $formRegister.find('.js-date');
        var $formRegisterTime = $formRegister.find('.js-time');

        $formRegisterDate.val($this.data('date'));
        $formRegisterTime.val($this.data('time'));

        $('html, body').animate({
            scrollTop: $$formRegister.offset().top
        }, 500);
    });
})();